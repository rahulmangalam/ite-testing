import { Component, OnInit } from '@angular/core';
import { Router ,NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit  {
  currentRoute = "";
  currentUser: any;

  constructor(
    private _router: Router,

  ) {}

  ngOnInit() {
    this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let url = event.url.split("?")[0];
        this.currentRoute = url.slice(1, 2).toUpperCase() + url.slice(2);
      }
    });
    // this._authService.currentUser.subscribe(user => {
    //   this.currentUser = user;
    // });
  }

  logout(reason?: string) {
    // this._authService.logout();
    this._router.navigate(["/home"]);
  }
}
