import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import {
FormBuilder,
FormControl,
FormGroup,
Validators,
NgForm
} from "@angular/forms";
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[AuthService]
})
export class LoginComponent implements OnInit {
   tittle="welcome to ";
  adminForm: FormGroup;
  mailId="rahul@gmail.com";
  type="admin";

  data1=[ {"type":"heading",
  "text":"Intelligent Territory Engagement",
  "mar_l":"12%",
  "fontsize": "xx-large",
  "fontweigth":"700",
  "font-style": "oblique",
  "color":"",
  "background":""
  },
  {"type":"button",
  "label": "know more",
  "fontsize": "20px",
  "color":"pink",
  "background":"",
  "mar_l":"12%",
  "event": "click",
  "action": "submit(i)",
  }]

  data=[
    {"type":"heading",
    "text": "welcome to ITR",
    "fontsize": "25px",
    "fontweigth":"400",
    "color":"white",
    "background":""
    },
    {"type":"para",
    "text": "please login",
    "fontsize": "20px",
    "color":"white",
    "background":""
    },

    {"type":"input",
    "labelName": "Email",
    "name":"e_mail",
    "inputType": "email",
    "placeholder": "Enter Your Email",
    "id": "input_email",
    "formControlName": "inputEmail",
    "required": "yes"
    },
    {"type":"input",
    "name":"pwd",
    "labelName": "Password",
    "inputType": "password",
    "placeholder": "Enter your password",
    "id": "input_pwd",
    "formControlName": "inputPassword",
    "required": "yes"
    },
    {
      "type":"button",
    "label": "submit",
    "event": "click",
    "action": "submit(i)",
    }

  ];
  formData:any;
  public loginForm: any;

  constructor(
    private route: ActivatedRoute,
    private Auth: AuthService,
    private router: Router
  ) {
  }

  ngOnInit() {


  }


  onSubmit(f: NgForm) {
    console.log(f.value);  // { first: '', last: '' }
    console.log(f.valid);  // false
  }
  login(){

    this.Auth.setLoggedIn(true,this.type, this.mailId)
    // localStorage.setItem('name', userInfo['docs'][0]['name'])





  }

  getdata(f:NgForm){
    console.log(f.value);
    if(true)
    this.router.navigate(['/student']);
    // else
    // this.router.navigate(['/admin']);
  }







}


