import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  data=[
    {"type":"heading",
    "text": "welcome to admin",
    "fontsize": "25px",
    "fontweigth":"400",
    "color":"black",
    "background":""
    },
    {
      "type":"button",
    "label": "about",
    "event": "click",
    "action": "submit(i)",
    "mar":"2%"
    },
    {
      "type":"button",
    "label": "settings ",
    "event": "click",
    "action": "submit(i)",
    "mar":"2%"
    },
    {
      "type":"button",
    "label": "submit",
    "event": "click",
    "action": "submit(i)",
    "mar":"2%"
    },

  ];

  constructor() {

   }

  ngOnInit()
   {

  }

}
