import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  data=[
    {"type":"heading",
    "text": "welcome to Student",
    "fontsize": "25px",
    "fontweigth":"400",
    "color":"black",
    "background":""
    },

    {
      "type":"button",
    "label": "about",
    "event": "click",
    "action": "submit(i)",
    "mar":"2%"
    },
    {
      "type":"button",
    "label": "report ",
    "event": "click",
    "action": "submit(i)",
    "mar":"2%"
    },
    {
      "type":"button",
    "label": "detail",
    "event": "click",
    "action": "submit(i)",
    "mar":"2%"
    },

  ];



  constructor() { }

  ngOnInit() {
  }

}
